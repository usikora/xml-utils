# Tree of Quotes
> A very brief introduction to XML (in english)

## What is this about?
The Tree of Quotes is an XML File ment to provide a gentle first intrduction to XML. It contains descriptions of the main concepts like
- comments
- processing instructions
- elements
- attributes
- namespaces

It also provides links to other introductions that go more into details.


## How to use it?
Just open the file and read it. You may also try XML yourself by editing the file. 