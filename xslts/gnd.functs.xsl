<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:gndutils="https://sub.uni-goettingen.de/met/xslt/functions/gnd"
    xmlns:err="http://www.w3.org/2005/xqt-errors" 
    exclude-result-prefixes="xs err"
    version="3.0">
    
    <!-- 
        How to use:
        ***********
        1) Add xmlns:gndutils="https://sub.uni-goettingen.de/met/xslt/functions/gnd" to the namespaces of your xslt
        2) import "https://gitlab.gwdg.de/usikora/xml-utils/-/raw/master/xslts/gnd.functs.xsl" to your xslt
            via <xsl:import href="https://gitlab.gwdg.de/usikora/xml-utils/-/raw/master/xslts/gnd.functs.xsl"/>
        3) Have fun using the GND
    -->
    
    <!-- Testing BOX - START -->
    <!--<xsl:output exclude-result-prefixes="err" indent="true"></xsl:output>
    <xsl:template match="/">
        <xsl:copy-of select="gndutils:marcxml-by-gnd-id('4021477-1')"></xsl:copy-of>
        <xsl:copy-of select="gndutils:marcxml('https://d-nb.info/gnd/4021477-1')"></xsl:copy-of>
    </xsl:template>-->
    <!-- Testing BOX - END -->
    
    <xsl:function name="gndutils:marcxml-by-gnd-id">
        <xsl:param name="gnd-id" />
        <xsl:copy-of select="gndutils:marcxml(concat('https://d-nb.info/gnd/', $gnd-id))"></xsl:copy-of>
    </xsl:function>
    
    <xsl:function name="gndutils:marcxml">
        <xsl:param name="gnd-uri" />
        <xsl:variable name="gnd-marc-url"><xsl:value-of select="$gnd-uri"/><xsl:text>/about/marcxml</xsl:text></xsl:variable>
        <xsl:try>
            <xsl:copy-of select="doc($gnd-marc-url)"></xsl:copy-of>
            <xsl:catch errors="*">
                <error code="{$err:code}">
                    <description><xsl:value-of select="$err:description"/></description>
                </error>
            </xsl:catch>
        </xsl:try>
    </xsl:function>
   
</xsl:stylesheet>