<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:wutils="https://sub.uni-goettingen.de/met/xslt/functions/wiki"
    exclude-result-prefixes="xs"
    version="3.0">
        
    <xsl:function name="wutils:wikipedia2gnd-id">
        <xsl:param name="wiki-url" />
        <xsl:variable name="wiki-base-url" select="wutils:get-wiki-base-url($wiki-url)"></xsl:variable>
        <xsl:variable name="wiki-export-url">
            <xsl:value-of select="$wiki-base-url"/><xsl:text>/w/index.php?title=Spezial:Exportieren&amp;pages=</xsl:text><xsl:value-of select="tokenize($wiki-url, '/')[last()]"/><xsl:text>&amp;offset=1&amp;limit=1&amp;action=submit</xsl:text>
        </xsl:variable>
        <xsl:variable name="wiki-xml" select="string-join((doc($wiki-export-url)//text()), '')"/>
        <xsl:variable name="gnd-id" select="wutils:get-gnd-id-from-wikipage($wiki-xml)"/>
        <xsl:copy-of select="$gnd-id"/>
    </xsl:function>
        
    <xsl:function name="wutils:get-wiki-base-url">
        <xsl:param name="wiki-url" />
        <xsl:value-of select="substring-before($wiki-url, '/wiki')"/>
    </xsl:function>

    <xsl:function name="wutils:get-gnd-id-from-wikipage">
        <xsl:param name="wiki-raw" />
        <!-- {{Normdaten|TYP=p|GND=1025785495|VIAF=284548186}} -->
        <xsl:analyze-string select="$wiki-raw" regex="GND=(.+)\|+?">
            <xsl:matching-substring>
                <xsl:value-of select="regex-group(1)"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring/>
        </xsl:analyze-string>
    </xsl:function>
    
    
    
</xsl:stylesheet>